package ru.t1.panasyuk.tm.api.model;

import ru.t1.panasyuk.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}