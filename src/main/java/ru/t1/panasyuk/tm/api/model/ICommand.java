package ru.t1.panasyuk.tm.api.model;

public interface ICommand {

    void execute();

    String getArgument();

    String getDescription();

    String getName();

}