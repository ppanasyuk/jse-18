package ru.t1.panasyuk.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Delete all tasks.";

    private static final String NAME = "task-clear";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}